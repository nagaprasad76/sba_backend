﻿using ProjectManager.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;


namespace ProjectManager.DataLayer
{
    public class ProjectContext : DbContext
    {
        public ProjectContext() : base("name = DBConnString")
        {
            Database.SetInitializer(new ProjectDBInitializer<ProjectContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ParentTask> ParentTasks { get; set; }

        private class ProjectDBInitializer<T> : CreateDatabaseIfNotExists<ProjectContext>
        {
            protected override void Seed(ProjectContext context)
            {
                var users = new List<User>
                    {
                    new User { FirstName="Manager1",LastName="A",EmployeeID=451582},
                    new User { FirstName="Manager2",LastName="B",EmployeeID=261428},
                    new User { FirstName="Manager3",LastName="C",EmployeeID=589745}
                    };
                foreach (User user in users)
                    context.Users.Add(user);
                context.SaveChanges();

                var parentTasks = new List<ParentTask>
                    {
                      new ParentTask {ParentTaskName = "Project Activity"},
                      new ParentTask {ParentTaskName = "Managerial Activity"},
                      new ParentTask {ParentTaskName = "Extra Curricular"}
                    };
                parentTasks.ForEach(s => context.ParentTasks.Add(s));
                context.SaveChanges();

                var projects = new List<Project>
                    {
                      new Project {ProjectName="MetLife", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30), Priority=15, UserID = context.Users.Single(u => u.FirstName=="Manager1").UserID},
                      new Project {ProjectName="Amex", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30), Priority=15, UserID=context.Users.Single(u => u.FirstName=="Manager2").UserID},
                      new Project {ProjectName="TD Bank", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30), Priority=15, UserID=context.Users.Single(u => u.FirstName=="Manager3").UserID}

                    };
                projects.ForEach(s => context.Projects.Add(s));
                context.SaveChanges();

                var tasks = new List<Task>
                    {
                      new Task {ParentID = context.ParentTasks.Single(p=>p.ParentTaskName=="Project Activity").ParentID , ProjectID = context.Projects.Single(p=>p.ProjectName=="MetLife").ProjectID,
                           TaskName ="Coding", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30),
                           Priority =15, Status=true, UserID = context.Users.Single(u => u.FirstName=="Manager1").UserID},
                      new Task {ParentID = context.ParentTasks.Single(p=>p.ParentTaskName=="Project Activity").ParentID , ProjectID = context.Projects.Single(p=>p.ProjectName=="Amex").ProjectID,
                           TaskName ="Deployment", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30),
                           Priority =15, Status=true, UserID = context.Users.Single(u => u.FirstName=="Manager2").UserID},
                      new Task {ParentID = context.ParentTasks.Single(p=>p.ParentTaskName=="Extra Curricular").ParentID , ProjectID = context.Projects.Single(p=>p.ProjectName=="TD Bank").ProjectID,
                           TaskName ="Singing", StartDate = new DateTime(2019,09,29), EndDate=new DateTime(2019,10,30),
                           Priority =15, Status=true, UserID = context.Users.Single(u => u.FirstName=="Manager3").UserID}
                    };
                tasks.ForEach(t => context.Tasks.Add(t));
                context.SaveChanges();

                base.Seed(context);

            }

        }
    }
}
